###  Proposed Timeline
I had to make a hard pivot because of a lot struggle with some of the libraries I chose and the approach I took. 
- Week 1

  - [X] Determine external libraries
  
  - [x] Determine data structures to implement
    - What functionss does the ADT use?
    - *How* do they use those functions?
    - What is a rough graphical representation of them?
    - Are there different implementations of each? Which will be considered?
  
   - [X] Creater header files for desired data structures
   
   - [X] Familiarize myself with the libraries that will be used
   
   - [X] Brainstorm test cases
   
   - [X] Implement one of the data structures
    
   - [X] Begin tests if enough time
   
- Week 2

  ~~- [ ] Test implemented data structure from previous week (if not already done)~~

  - [ ] Start implementing graphical representation of previous data structure
    - At this time, assuming *most* of the visual repersentations will have similarities between them
   
  ~~- [ ] Implement second data structure~~
    ~~- Begin tests, or rough plan of testing~~
      - No longer implemting my own data structures due to complications/naivete
    
  - [ ] Integrate second data structure into a visual representation
  
- Week 3

  - [ ] Complete testing from previous week
  
 ~~- [ ] Implement third data structure~~
  
   ~~- Testing for this data structure should be completed, as well~~
    
  ~~- [ ] Integrate visual representation of third data structure~~
  
 ~~- [ ] If time allows, begin work on fourth data structure~~
  
##### Excess Time
No excess time.
~~In a situation where I found myself with time to spare, I will continue to refine my current implementations
(more functions, test cases?) and graphical representation. If I believe I have enough time, I may begin working on
yet another data structure.~~

