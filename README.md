# Data Structure Visualization

[![license](https://img.shields.io/badge/license-MIT-green)](LICENSE)
[![docs](https://img.shields.io/badge/docs-yes-brightgreen)](docs/README.md)


**Author**: [Mat Farley](mathewf2@illinois.edu)

---
### Goal and Purpose
The purpose of this project is to visually represent a variety of data structures with C++.
The purpose is to better understand how these data structures work myself, practice with implementing
data structures in a new language and (hopefully) helping someone in the future.

### Relevant Experience
I have experience implementing data structures in the past, but primarily with Java (and some Kotlin). Considering
the powerful nature of C++, I find it would be beneficial to reimplement such data structures using C++ and it's
conventions.

### External Libraries and Resources
At the time of writing these I am not dead set on any particular library. However, a few have seemed useful such as
~~[GTK+](https://www.gtk.org/), [Boost](https://github.com/boostorg), [Ogre 3D](https://www.ogre3d.org/), 
and the [Choreograph](https://github.com/sansumbrella/Choreograph) Cinderblock.~~ Decided on using Choreograph cinderblock and [ImGui](https://github.com/simongeilfus/Cinder-ImGui). A lot of fruitless time and effort was spent trying to make use of Choreograph, but was ended up unused. Pivoted to using Cinder's integrated graphics.

### Setbacks
I had *a lot* of trouble with this project, unfortunately. I wanted to accomplish something far beyond my breadth and outside the scope of the technology we were required to use (in a timely manner, at the least). I initially wanted to implement my own data structures and animate them with Choreograph. I have since dropped Choreograph, due to inability to make sense of the animations and lack of time, and have pivoted away from implementing my own data structures. The latter was primarily due to the approach I was taking, and my ignorance to C++ as a language and it's programming idoms. I decided to approach the data structure with, what I feel, was a very thoroughly object decomposition strategy. Unfortunately, due to my lack of experience with C++ I spent a lot of time trying to figure things out, rather than actually *programming*. I would love to reapproach this problem and be able to readdress the issue. Any feedback would be greatly appreciated, because it is these back-end, object oriented programming that I love to wrap my mind around.
  - ##### For reference, these are the OOP steps I have taken, and my mindset (a lot of these things I did not know how to approach with C++, wherein a lot of my problems arose)
    - Create an abstract class (ADTList.h) that represents common functionality between list interfaces (Vectors and Linked Lists in this instance)
    - I *wanted* to include an abstract iterator implementation in ADTList.h as well. I went back and forth a lot here, and thought of making a seperate Iterator class, but because the list-iterator relationship is representative of a composition relationship (has-a), rather than an inheritance relationship (is-a).
      - Thus, with some guidance from SO that I didn't entirely understand, I created a concrete Iterator class within ADTList.h, as well as a virtual iteratorImpl class, whose functions rely on the list implementation. There was also virtual begin()/end() functions. This choice in implementing iterators like this led to a huge time sink, and made me decide to pivot and use the stl vectors instead of my own data structures unfortunately.
    - I only got so far as implementing Vector. With experience implementing Linked Lists, I though that might be easy.
    - Vector *compiles* (finally), but immediately hits a segfault upon testing. I think I know why this is occuring but I decided against debugging and pivoting.
    - The Tree class remains almost entirely incomplete.
    - There are little nuances of C++ I am still struggling to wrap my mind around; primarily inclusion statements. (#include <Vector.h> in vector_tests.cc leads to "undefined references", whereas we did this for our previous linked list and it worked fine? I needed to #include "../src/Vector.cc" to compile, though I anticipated this to lead to multiple definitions?? Excuse my ignorance)
