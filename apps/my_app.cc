// Copyright (c) 2020 [Your Name]. All rights reserved.

#include "my_app.h"

#include <cinder/app/App.h>
#include <cinder/gl/gl.h>
#include <mylibrary/util.h>
#include <imgui_demo.cpp>

namespace myapp {

using cinder::app::KeyEvent;

MyApp::MyApp() = default;


void MyApp::setup() {
  
  ui::initialize(ui::Options().darkTheme()
  .windowMinSize(glm::vec2(1024, 512)));
  ui::Options().fontGlobalScale(3.0);
}

void MyApp::update() {
  
  static bool display_linked = false;
  static bool display_vector = false;
  
  cinder::gl::clear();

  if (display_linked) {
    displayLinkedList(linked, &display_linked);
    ui::End();
  }
  if (display_vector) {
    displayVector(vect, &display_vector);
    ui::End();
  }

  ui::ScopedWindow("Abstract List Visualization");
  
  if(ImGui::CollapsingHeader("Vector")) {
    if (ui::Button("Generate Random Vector")) {
      vect.clear();
      while(vect.size() < 5) {
        vect.push_back(std::rand() % 10 + 1);
      }
      display_vector = true;
    }
    
    static bool selected = false;
    ui::Selectable("Create Custom Vector", &selected);
    
    if (selected) {
      ui::BeginChild("Custom Vector", ImVec2(ui::GetWindowWidth(),
          ui::GetFontSize() * 5.0f));
      static std::string input;
      ui::TextWrapped("Enter 5 numbers separated by non-numeric characters");
      ui::InputText("", &input);
      if(ui::Button("Create Vector")) {
        std::cout << input << std::endl;
        vect = util::parseInput(input);
        display_vector = true;
      }
      ui::EndChild();
    }
  }
  
  if(ImGui::CollapsingHeader("Linked List")) {
    if (ui::Button("Generate Random Linked List")) {
      linked.clear();
      while(linked.size() < 5) {
        linked.push_back(std::rand() % 10 + 1);
      }
      std::cout << std::endl;
      display_linked = true;
    }
    
    static bool selected = false;
    ui::Selectable("Create Custom Linked List", &selected);
    
    if (selected) {
      ui::BeginChild("Custom Linked List", ImVec2(ui::GetWindowWidth(), 
          ui::GetFontSize() * 5.0f));
      static std::string input;
      ui::TextWrapped("Enter 5 numbers separated by non-numeric characters");
      ui::InputText("", &input);
      if (ui::Button("Create Linked List")) {
        std::cout << input << std::endl;
        linked = util::parseInput(input);
        display_linked = true;
      }
      ui::EndChild();
    }
  }
}

void MyApp::displayLinkedList(const std::vector<int>& list, bool* open) {
  linkedListDisplay.list = list;
  linkedListDisplay.window_width = getWindowWidth();
  linkedListDisplay.window_height = getWindowHeight();
  linkedListDisplay.center = getWindowCenter();
  linkedListDisplay.DrawLinkedList();
  linkedListDisplay.draw(open);
}

void MyApp::displayVector(const std::vector<int>& vec, bool* open) {
  vectDisplay.vect  = vec;
  vectDisplay.window_width = getWindowWidth();
  vectDisplay.window_height = getWindowHeight();
  vectDisplay.center = getWindowCenter();
  vectDisplay.DrawVector();
  vectDisplay.draw(open);
}

void MyApp::draw() {}

void MyApp::keyDown(KeyEvent event) {
  if(event.getCode() == KeyEvent::KEY_ESCAPE) {
    exit(1);
  }
}

void MyApp::linkedListDisplay::draw(bool* open = nullptr) {
  ui::Begin("Linked List Display", open);
  static char append_value[10];
  ui::InputText("Append Value", append_value, 10, ImGuiInputTextFlags_CharsDecimal);
  if(ui::Button("Add (append)")) {
    int append = std::stoi(append_value);
    DrawLinkedListAppend(append);
  }

  ui::NewLine();
  static char add_value[10];
  static char add_index[10];
  ui::InputText("Add Value", add_value, 10, ImGuiInputTextFlags_CharsDecimal);
  ui::InputText("Add Index", add_index, 10, ImGuiInputTextFlags_CharsDecimal);
  if(ui::Button("Add (by index")) {
    int value = std::stoi(add_value);
    int index = std::stoi(add_index);
    DrawLinkedListAdd(index, value);
  }

  ui::NewLine();
  static char get_index[10];
  ui::InputText("Get Index", get_index, 10, ImGuiInputTextFlags_CharsDecimal);
  if(ui::Button("Get (by index)")) {
    int index = std::stoi(get_index);
    DrawLinkedListGet(index);
  }

  ui::NewLine();
  static char remove_value[10];
  ui::InputText("Remove Value", remove_value, 10, ImGuiInputTextFlags_CharsDecimal);
  if (ui::Button("Remove (value)")) {
    int to_remove = std::stoi(remove_value);
    DrawLinkedListRemoveValue(to_remove);
  }

  ui::NewLine();
  static char remove_index[10];
  ui::InputText("Remove Index", remove_index, 10, ImGuiInputTextFlags_CharsDecimal);
  if (ui::Button("Remove (by index)")) {
    int index = std::stoi(remove_index);
    DrawLinkedListRemoveIndex(index);
  }
}
void MyApp::linkedListDisplay::DrawLinkedList() {
  size_t num_values = list.size();
  float width = window_width / (2.5f * num_values);
  float height = window_height / (2.5f * num_values);
  float  start_x = center.x / 5.0f;
  float start_y = 0.75f * center.y;
  
  cinder::gl::color(10, 0, 0);
  for (size_t i = 0; i < num_values; i++) {
    float first_x = start_x + (2.0f * i) * width;
    float second_x = start_x + width * ((2.0f * i) + 1);
    float first_y = start_y;
    float second_y = start_y + height;
    
    cinder::gl::drawStrokedRect(
        ci::Rectf(first_x, first_y,second_x,
            second_y), 12.5);
  }
  
  
  
}
void MyApp::linkedListDisplay::clearLinkedList() {}
void MyApp::linkedListDisplay::DrawLinkedListAppend(int value) {}
void MyApp::linkedListDisplay::DrawLinkedListAdd(int index, int value) {}
void MyApp::linkedListDisplay::DrawLinkedListGet(int index) {}
void MyApp::linkedListDisplay::DrawLinkedListRemoveValue(int value) {}
void MyApp::linkedListDisplay::DrawLinkedListRemoveIndex(int index) {}

void MyApp::vectorDisplay::draw(bool* open = nullptr) {
  ui::Begin("Vector Display", open);

  static char append_value[10];
  ui::InputText("Append Value", append_value, 10, ImGuiInputTextFlags_CharsDecimal);
  if(ui::Button("Add (append)")) {
    int append = std::stoi(append_value);
    DrawVectorAppend(append);
  }
  
  ui::NewLine();
  static char add_value[10];
  static char add_index[10];
  ui::InputText("Add Value", add_value, 10, ImGuiInputTextFlags_CharsDecimal);
  ui::InputText("Add Index", add_index, 10, ImGuiInputTextFlags_CharsDecimal);
  if(ui::Button("Add (by index")) {
    int add = std::stoi(add_value);
    int index = std::stoi(add_index);
    DrawVectorAdd(index, add);
  }

  ui::NewLine();
  static char get_index[10];
  ui::InputText("Get Index", get_index, 10, ImGuiInputTextFlags_CharsDecimal);
  if(ui::Button("Get (by index)")) {
    int index = std::stoi(get_index);
    DrawVectorGet(index);
  }
  
  ui::NewLine();
  static char remove_value[10];
  ui::InputText("Remove Value", remove_value, 10, ImGuiInputTextFlags_CharsDecimal);
  if (ui::Button("Remove (value)")) {
    int to_remove = std::stoi(remove_value);
    DrawVectorRemoveValue(to_remove);
  }
  
  ui::NewLine();
  static char remove_index[10];
  ui::InputText("Remove Index", remove_index, 10, ImGuiInputTextFlags_CharsDecimal);
  if (ui::Button("Remove (by index)")) {
    int index = std::stoi(remove_index);
    DrawVectorRemoveIndex(index);
  }

}
void MyApp::vectorDisplay::DrawVectorAppend(int value) {}
void MyApp::vectorDisplay::DrawVectorAdd(int index, int value) {}
void MyApp::vectorDisplay::DrawVectorGet(int index) {}
void MyApp::vectorDisplay::DrawVectorRemoveValue(int value) {}
void MyApp::vectorDisplay::DrawVectorRemoveIndex(int index) {}
void MyApp::vectorDisplay::DrawVector() {}
void MyApp::vectorDisplay::clearVector() {}
}  // namespace myapp
