// Copyright (c) 2020 CS126SP20. All rights reserved.

#ifndef FINALPROJECT_APPS_MYAPP_H_
#define FINALPROJECT_APPS_MYAPP_H_

#include <CinderImGui.h>
#include <cinder/app/App.h>
//#include <mylibrary/DataStructures/Tree.h>
//#include <mylibrary/DataStructures/Lists/Vector.h>
//#include <mylibrary/DataStructures/Lists/LinkedList.h>

namespace myapp {

class MyApp : public cinder::app::App {
  
  // Struct representing the linked list display
  // Uses parameters of the window to automatically resize display
  // draw() is the primary draw function; the others are called from within
  // draw, and are named after their ADT function
  // Can (maybe should?) be refactored into classes; unfamiliar with
  // where struct fall within OOP.
  struct linkedListDisplay {
    std::vector<int> list;
    cinder::vec2 center;
    int window_width;
    int window_height;
    
    void draw(bool* open);
    void DrawLinkedList();
    void clearLinkedList();
    void DrawLinkedListAppend(int value);
    void DrawLinkedListAdd(int index, int value);
    void DrawLinkedListGet(int index);
    void DrawLinkedListRemoveValue(int value);
    void DrawLinkedListRemoveIndex(int index);
    
  };
  
  // Struct representing the vector display
  // Uses parameters of the window to automatically resize display
  // draw() is the primary draw function; the others are called from
  // within draw(), and are named after their ADT function
  // Can (maybe should?) be refactored into classes; unfamiliar with
  // where struct fall within OOP.
  struct vectorDisplay {
    std::vector<int> vect;
    cinder::vec2 center;
    int window_width;
    int window_height;
    
    void draw(bool* open);
    void DrawVector();
    void clearVector();
    void DrawVectorAppend(int value);
    void DrawVectorAdd(int index, int value);
    void DrawVectorGet(int index);
    void DrawVectorRemoveValue(int value);
    void DrawVectorRemoveIndex(int index);
  };
  
 public:
  // Vectors used to store the data being visualized
  std::vector<int> linked;
  std::vector<int> vect;
  
  // Structs for respresenting the visualization
  vectorDisplay vectDisplay;
  linkedListDisplay linkedListDisplay;
  
  MyApp();
  
  // Initializes ImGui, sets the theme, size and font
  void setup() override;
  
  // Generates the main menu and display different menu choices based on the
  // user selection
  void update() override;
  
  // Currently unused
  void draw() override;
  
  // Escape key exits the program
  void keyDown(cinder::app::KeyEvent) override;
  
  // Functions used to initialize struct values and begin
  // visualization process
  void displayLinkedList(const std::vector<int>& ll, bool* display);
  void displayVector(const std::vector<int>& vec, bool* open);
};

}  // namespace myapp

#endif  // FINALPROJECT_APPS_MYAPP_H_
