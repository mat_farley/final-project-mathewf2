//
// Created by mat on 5/1/20.
//

#ifndef FINALPROJECT_ADTLIST_H
#define FINALPROJECT_ADTLIST_H

#include <vector>


// An abstract class (namespace?) representing common functionality of lists
// (Primarily vectors and linked lists considered for implementation purposes)

namespace adtlist {

template <typename ElementType>
 class ADTList {
   
   // List-iterator represents a composition relation, which led to my design
   // choice of having iterators be nested within the ADTList interface.
   // A lot of pain and confusion arose from this design choice.
   // Refer to README for more (too much) detail.
  
   // Guidance taken from SO on proper object composition for iterators, 
   // https://stackoverflow.com/questions/13304415/c-abstract-class-with-nested-class-derived-class-and-nested-class
   // And the pImpl idiom, 
   // https://en.wikibooks.org/wiki/C++_Programming/Idioms#Pointer_To_Implementation_.28pImpl.29
   
   // Virtual iterator implementation
  protected:
   class iteratorImpl {
   public:
    virtual void next() = 0;
    virtual void next() const = 0;
    virtual ElementType& get() const = 0;
  };
   
   // Concrete iterator class reliant on iteration implementations
  public:
   class iterator : std::iterator<std::forward_iterator_tag, ElementType> {
     iteratorImpl* current_;
     
    public:
     iterator() : current_(nullptr){};
     explicit iterator(iteratorImpl* impl) : current_(impl) {};
     ~iterator() { delete current_; }
     bool operator != (const iterator& other) const {
       return current_ != other.current_;
     }
     iterator& operator++ () {
       current_->next();
       return *this;
     }
     ElementType& operator*() const {
       return current_->get();
     }
   };
   
   // Returns the iterator at the first location within the list
   virtual iterator begin() = 0;
   // Returns the iterator at the last location within the list
   virtual iterator end() = 0;
   
   // Constant iterator, function most of the same as non-constant variant
   class const_iterator : std::iterator<std::forward_iterator_tag, ElementType> {
     const iteratorImpl* current_;
     
    public:
     const_iterator() : current_(nullptr) {};
     explicit const_iterator(iteratorImpl* impl) : current_(impl) {};
     ~const_iterator() { delete current_; }
     bool operator != (const const_iterator & other) const {
       return current_ != other.current_;
     }
       const_iterator& operator++() {
         current_->next();
         return *this;
       }
       ElementType& operator*() const {
         return current_->get();
       }
   };

   // Returns the iterator at the first location within the list
   virtual const_iterator begin() const = 0;
   // Returns the iterator at the last location within the list
   virtual const_iterator end() const = 0;
   
  virtual ~ADTList() = default;;

  // Appends value to end of list
  virtual void add(const ElementType& value) = 0;

  // Adds value at desired index
  virtual void add(size_t index, ElementType& value) = 0;

  // Determines whether element exists within the list
  virtual bool contains(const ElementType& value) = 0;

  // Returns the size of the list
  virtual size_t size() const = 0;

  // Determines whether the list is empty or not
  virtual bool isEmpty() = 0;

  // Clears content of list
  virtual void clear() = 0;

  // Returns first element of list
  virtual ElementType front() = 0;

  // Returns last element of list
  virtual ElementType back() = 0;

  // Get the element at desired index
  virtual ElementType get(size_t index) = 0;

  // Removes the element at desired location; returns true if successful,
  // false otherwise
  virtual bool removeAt(size_t index) = 0;
  
  // Removes the first occurrence of the desired value; returns true if
  // successful, false otherwise
  virtual bool remove(const ElementType& value) = 0;

  // Operator to determine if two lists are equivalent
  virtual bool operator==(const ADTList<ElementType>& rhs) const = 0;
  
  // Operator to determine if two lists are inequivalent
  virtual bool operator!=(const ADTList<ElementType>& rhs) const = 0;
  
 };
}

#endif  // FINALPROJECT_ADTLIST_H
