//
// Created by mat on 5/1/20.
//

#ifndef FINALPROJECT_LINKEDLIST_H
#define FINALPROJECT_LINKEDLIST_H

#include <vector>
#include "ADTList.h"

// Currently unimplemented, and out dated. ADTList.h has since been refactored

using namespace adtlist;

namespace linkedlist {

template <typename ElementType>
class LinkedList : public ADTList<ElementType> {
  
  struct Node {
    Node* next = nullptr;
    ElementType value;
  };

  Node* head_ = nullptr;
  Node* tail_ = nullptr;
  int size_ = 0;
 
 public:
  
  LinkedList();
  
  explicit LinkedList(ADTList<ElementType>);
  
  LinkedList(std::vector<ElementType>);
  
  void add(const ElementType& value) override;
  
  void add(int index, const ElementType& value) override;
  
  bool contains(ElementType value) override;
  
  int size() override;
  
  bool isEmpty() override;
  
  void clear() override;
  
  ElementType front() override;
  
  ElementType back() override;
  
  ElementType get(int index) override;
  
  bool removeAt(int index) override;
  
  bool remove(const ElementType& value) override;
  
  bool operator==(const ADTList<ElementType>& rhs) const override;
  ~LinkedList() override;
};

}


#endif  // FINALPROJECT_LINKEDLIST_H
