//
// Created by mat on 5/1/20.
//

#ifndef FINALPROJECT_VECTOR_H
#define FINALPROJECT_VECTOR_H

#include <iostream>
#include "ADTList.h"



// Vector.cc is to a point of compiling, but there are errors within the
// implementation; pivoted away from own implementations at this point
namespace vector {

template <typename ElementType>

class Vector : public adtlist::ADTList<ElementType> {
  // Internal states storing the size and array associated with the vector
  size_t size_;
  ElementType* array_;

 protected:
  class iteratorImpl : public adtlist::ADTList<ElementType>::iteratorImpl {
    // An *attempt* at trying to create an iterator implementation using
    // an index and array (based on many SO examples..)
    size_t impl_index_ = 0;
    ElementType* impl_arr_;
    
    public:
    explicit iteratorImpl(ElementType* arr) : impl_arr_(arr) {};
    void next() override;
    void next() const override;
    ElementType& get() const override;
    //bool isEqual(const iteratorImpl& other) const override { return current }
  };
  
 public:
  Vector() : size_(0), array_(new ElementType[0]) {};

  ~Vector() override;

  explicit Vector(const adtlist::ADTList<ElementType>&);
  
  explicit Vector(const std::vector<ElementType>&);
  
  void add(const ElementType& value) override;
  
  void add(size_t index, ElementType& value) override;
  
  bool contains(const ElementType& value) override;

  size_t size() const override;
  
  bool isEmpty() override;
  
  void clear() override;
  
  ElementType front() override;
  
  ElementType back() override;
  
  ElementType get(size_t index) override;
  
  bool removeAt(size_t index) override;
  
  bool remove(const ElementType& value) override;
  
  bool operator==(const adtlist::ADTList<ElementType>& rhs) const override;

  bool operator!=(const adtlist::ADTList<ElementType>& rhs) const override;

  typename adtlist::ADTList<ElementType>::iterator begin() override;

  typename adtlist::ADTList<ElementType>::iterator end() override;

  typename adtlist::ADTList<ElementType>::const_iterator begin() const override;
  
  typename adtlist::ADTList<ElementType>::const_iterator end() const override;
};

template <typename ElementType>
std::ostream& operator<<(std::ostream& os, const Vector<ElementType>& vec);
}

#endif  // FINALPROJECT_VECTOR_H
