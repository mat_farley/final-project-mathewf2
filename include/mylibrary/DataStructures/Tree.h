//
// Created by mat on 5/1/20.
//

#ifndef FINALPROJECT_TREE_H
#define FINALPROJECT_TREE_H

#include <mylibrary/DataStructures/Lists/ADTList.h>
#include <vector>

// Everything related to tree is currently unimplemented

namespace tree {

template <typename ElementType>
class Tree {
  
  struct Leaf {
    Leaf* left;
    Leaf* right;
    ElementType value;
  };
  
  Leaf root_;
  int size_ = 0;
  
  Tree();
  
  Tree(adtlist::ADTList<ElementType>);
  
  Tree(std::vector<ElementType>);
  
  ~Tree();

  int size();

  void insert(ElementType value);
  
  bool contains(ElementType value);
  
  int height();
  
  bool remove(ElementType value);
  
  void preorder();
  
  void inorder();
  
  void postorder();
  
  void binary_search();
  
  void depth_first_search();
  
  void breadth_first_search();
  
  
};

}

#endif  // FINALPROJECT_TREE_H
