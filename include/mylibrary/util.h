//
// Created by mat on 5/5/20.
//

#ifndef FINALPROJECT_UTIL_H
#define FINALPROJECT_UTIL_H

#include <vector>
#include <string>
#include <iostream>

// Utility namespace to store miscellaneous functions

namespace util {

  // Parses the usesrs input of a string into a vector of ints
  // it treats all non-numeric characters the same, and treats them all
  // as delimiters between numeric values. Currently not properly
  // tested
  std::vector<int> parseInput(const std::string& input);
}

#endif  // FINALPROJECT_UTIL_H
