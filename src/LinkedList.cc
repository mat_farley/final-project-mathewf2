//
// Created by mat on 5/1/20.
//

#include <mylibrary/DataStructures/Lists/LinkedList.h>

// Currently unimplemented, and out dated. ADTList.h has since been refactored

namespace linkedlist {

template <typename ElementType>
LinkedList<ElementType>::LinkedList() {
  size_ = 0;
}

template <typename ElementType>
LinkedList<ElementType>::LinkedList(ADTList<ElementType> values) {
  
}

template <typename ElementType>
LinkedList<ElementType>::LinkedList(std::vector<ElementType>) {
  
}

template <typename ElementType>
LinkedList<ElementType>::~LinkedList() = default;

template <typename ElementType>
void LinkedList<ElementType>::add(const ElementType& value) {
  size_++;
}

template <typename ElementType>
void LinkedList<ElementType>::add(int index, const ElementType& value) {
  size_++;
}

template <typename ElementType>
bool LinkedList<ElementType>::contains(ElementType value) {
  return false;
}

template <typename ElementType>
int LinkedList<ElementType>::size() {
  return 0;
}

template <typename ElementType>
bool LinkedList<ElementType>::isEmpty() {
  return false;
}

template <typename ElementType>
void LinkedList<ElementType>::clear() {
  
}

template <typename ElementType>
ElementType LinkedList<ElementType>::front() {
  return nullptr;
}

template <typename ElementType>
ElementType LinkedList<ElementType>::back() {
  return nullptr;
}

template <typename ElementType>
ElementType LinkedList<ElementType>::get(int index) {
  return nullptr;
}

template <typename ElementType>
bool LinkedList<ElementType>::removeAt(int index) {
  return false;
}

template <typename ElementType>
bool LinkedList<ElementType>::remove(const ElementType& value) {
  return false;
}

template <typename ElementType>
bool LinkedList<ElementType>::operator==(
    const ADTList<ElementType>& rhs) const {
  return false;
}

}
