//
// Created by mat on 5/1/20.
//

#include <mylibrary/DataStructures/Tree.h>

// Everything related to tree is currently unimplemented

namespace tree {

template <typename ElementType>
Tree<ElementType>::Tree() = default;

template <typename ElementType>
Tree<ElementType>::Tree(adtlist::ADTList<ElementType>) {
  
}

template <typename ElementType>
Tree<ElementType>::Tree(std::vector<ElementType>) {
  
}

template <typename ElementType>
Tree<ElementType>::~Tree() = default;

template <typename ElementType>
int Tree<ElementType>::size() {
  return 0;
}

template <typename ElementType>
void Tree<ElementType>::insert(ElementType value) {
  
}

template <typename ElementType>
bool Tree<ElementType>::contains(ElementType value) {
  return false;
}

template <typename ElementType>
int Tree<ElementType>::height() {
  return 0;
}

template <typename ElementType>
bool Tree<ElementType>::remove(ElementType value) {
  return false;
}

template <typename ElementType>
void Tree<ElementType>::preorder() {
  
}

template <typename ElementType>
void Tree<ElementType>::inorder() {
  
}

template <typename ElementType>
void Tree<ElementType>::postorder() {
  
}

template <typename ElementType>
void Tree<ElementType>::binary_search() {
  
}

template <typename ElementType>
void Tree<ElementType>::depth_first_search() {
  
}

template <typename ElementType>
void Tree<ElementType>::breadth_first_search() {
  
}

}
