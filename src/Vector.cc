//
// Created by mat on 5/1/20.
//

#include <mylibrary/DataStructures/Lists/Vector.h>

using adtlist::ADTList;

namespace vector {

template <typename ElementType>
Vector<ElementType>::Vector(const adtlist::ADTList<ElementType>& values) {
  size_t size = values.size();
  auto* temp_array = new ElementType[size];
  for (size_t i = 0; i < values.length; i++) {
    temp_array[i] = values[i];
  }
  array_ = temp_array;
  size_ = size;
  delete[] temp_array;
}

template <typename ElementType>
Vector<ElementType>::Vector(const std::vector<ElementType>& values) {
  size_t size = values.size();
  auto* temp_array = new ElementType[size];
  for (size_t i = 0; i < values.size(); i++) {
    temp_array[i] = values[i];
  }
  array_ = temp_array;
  size_ = size;
  delete[] temp_array;
}

template <typename ElementType>
Vector<ElementType>::~Vector() {
  clear();
}

template <typename ElementType>
void Vector<ElementType>::add(const ElementType& value) {
  auto* temporary_array = new ElementType[size_];
  for (size_t i = 0; i < size_; i++) {
    temporary_array[i] = array_[i];
  }
  temporary_array[size_ - 1] = value;
  delete[] array_;
  array_ = temporary_array;
  delete[] temporary_array;
  size_++;
  
}

template <typename ElementType>
void Vector<ElementType>::add(size_t index, ElementType& value) {
  if (index < 0 || index > size_) {
    return;
  }
  size_++;
  auto* temporary_array = new ElementType*[size_];
  size_t old_index = 0;
  for (size_t new_index = 0; new_index < size_ ; new_index++) {
    if (new_index == index) {
      temporary_array[index] = &value;
      new_index++;
    }
    if (old_index < size_ - 1) {
      temporary_array[new_index] = &array_[old_index];
      old_index++;
    }
  }
  delete[] array_;
  array_ = *temporary_array;
  delete[] temporary_array;
}

template <typename ElementType>
bool Vector<ElementType>::contains(const ElementType& value) {
  for (auto itr = begin(); itr != end(); ++itr) {
    if(*itr == value) {
      return true;
    }
  }
  return false;
  
}

template <typename ElementType>
size_t Vector<ElementType>::size() const {
  return size_;
}

template <typename ElementType>
bool Vector<ElementType>::isEmpty() {
  return size_ == 0;
}

template <typename ElementType>
void Vector<ElementType>::clear() {
  delete[] array_;
  array_ = nullptr;
}

template <typename ElementType>
ElementType Vector<ElementType>::front() {
  return array_[0];
}

template <typename ElementType>
ElementType Vector<ElementType>::back() {
  return (array_[size_ - 1]);
}

template <typename ElementType>
ElementType Vector<ElementType>::get(size_t index) {
  return array_[index];
}

template <typename ElementType>
bool Vector<ElementType>::removeAt(size_t index) {
  if (index < 0 || index >= size_) {
    return false;
  }
  size_--;
  auto* temporary_array = new ElementType*[size_];
  size_t original_index = 0;
  for (size_t new_index = 0; new_index < size_; new_index++) {
    if(new_index == index) {
      original_index++;
    }
    temporary_array[new_index] = &array_[original_index];
    original_index++;
  }
  delete[] array_;
  array_ = *temporary_array;
  delete[] temporary_array;
}

template <typename ElementType>
bool Vector<ElementType>::remove(const ElementType& value) {
  if (size_ < 1) {
    return false;
  }
  for (size_t i = 0; i < size_; i++) {
    if (array_[i] == value) {
      removeAt(i);
      return true;
    }
  }
  return false;
}

template <typename ElementType>
bool Vector<ElementType>::operator==(const adtlist::ADTList<ElementType>& rhs) const {
  if (rhs.size() != size_) {
    return false;
  }
  for (auto first_itr = begin(), second_itr = rhs.begin();
  first_itr != end() && second_itr != rhs.end();
  ++first_itr, ++second_itr) {
    if (*first_itr != *second_itr) {
      return false;
    }
  }
  return true;
}

template <typename ElementType>
bool Vector<ElementType>::operator!=(
    const adtlist::ADTList<ElementType>& rhs) const {
  if (rhs.size() != size()) {
    return true;
  }

  return !(operator==(rhs));
}


template<typename ElementType>
typename adtlist::ADTList<ElementType>::iterator Vector<ElementType>::begin(){
return typename adtlist::ADTList<ElementType>::iterator(new iteratorImpl(array_));
}
template<typename ElementType>
typename adtlist::ADTList<ElementType>::iterator Vector<ElementType>::end(){
return typename adtlist::ADTList<ElementType>::iterator(new iteratorImpl(array_ + size_)); 
}
template<typename ElementType>
typename adtlist::ADTList<ElementType>::const_iterator Vector<ElementType>::begin() const{
return typename adtlist::ADTList<ElementType>::const_iterator(new iteratorImpl(array_));
}
template<typename ElementType>
typename adtlist::ADTList<ElementType>::const_iterator Vector<ElementType>::end() const{
return typename adtlist::ADTList<ElementType>::const_iterator(new iteratorImpl(array_ + size_));
}

template<typename ElementType>
void Vector<ElementType>::iteratorImpl::next() {
  if (impl_arr_ + impl_index_ != nullptr) {
    impl_index_++;
  }
}template<typename ElementType>
ElementType& Vector<ElementType>::iteratorImpl::get() const{
  return impl_arr_[impl_index_];
}
template <typename ElementType>
void Vector<ElementType>::iteratorImpl::next() const {
  if (impl_arr_ + impl_index_ != nullptr) {
//    impl_index_++;
  }
}

template <typename ElementType>
std::ostream& operator<<(std::ostream& os, const Vector<ElementType>& vec) {
  std::string to_return = "[";
  for (auto itr = vec.begin(); itr != vec.end(); ++itr) {
    to_return += std::to_string(*itr) + ", ";
  }
  to_return += "]";
  os<< to_return;
  return os;
  
}

}  // namespace vector