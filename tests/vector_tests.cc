//
// Created by mat on 5/6/20.
//

#define CATCH_CONFIG_MAIN

#include <cinder/Rand.h>
#include "../src/Vector.cc"

#include <catch2/catch.hpp>


using vector::Vector;

TEST_CASE("Random sanity test", "[random]") {
const float random = cinder::randFloat();
REQUIRE(0. <= random);
REQUIRE(random <= 1.);
}

// Immediately throws a fault; pretty sure I know exactly where with my
// destructors. For sake for time, rest of the test cases left
// unimplemented
TEST_CASE("Simple add") {
  Vector<int> vec = Vector<int>();
  vec.add(1);
  std::cout << vec << std::endl;
}